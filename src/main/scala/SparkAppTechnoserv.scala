import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.sql.{DataFrame, GroupedData, SQLContext}
import org.apache.spark.sql.functions._


object SparkAppTechnoserv {

  val PATH_TO_DATA = "file:///home/cloudera/data/"

  val TEMP_BY_CITY_FILE: String = PATH_TO_DATA + "GlobalLandTemperaturesByCity.csv"
  val TEMP_BY_COUNTRY_FILE: String = PATH_TO_DATA + "GlobalLandTemperaturesByCountry.csv"
  val TEMP_GLOBAL_FILE: String = PATH_TO_DATA + "GlobalTemperatures.csv"

  val OUTPUT_PATH: String = "hdfs://quickstart.cloudera:8020/tmp/res.parquet"


  def loadDataFrame(path: String)(implicit sqlContext: SQLContext): DataFrame = {
    import sqlContext.implicits._
    val df = sqlContext.read.format("com.databricks.spark.csv")
      .option("header", "true")
      .option("inferSchema", "true")
      .load(path)

    df
      .withColumn("dt", to_date($"dt").cast("date"))
      .withColumn("year", year($"dt"))
      .withColumn("decade", $"year".divide(10).cast("integer"))
      .withColumn("century", $"year".divide(100).cast("integer"))
  }

  /**
    * This function aggregates
    */
  def aggregateGlobal(groupedData: GroupedData, prefix: String, suffix: String)(implicit sqlContext: SQLContext): DataFrame = {
    import sqlContext.implicits._
    groupedData.agg(
      avg($"LandAverageTemperature").alias(s"${prefix}AverageTemperature${suffix}"),
      max($"LandMaxTemperature").alias(s"${prefix}MaxTemperature${suffix}"),
      min($"LandMinTemperature").alias(s"${prefix}MinTemperature${suffix}")
    )
  }

  /**
    * This function aggregates temperature if only AverageTemperature is known
    */
  def aggregateLocal(groupedData: GroupedData, prefix: String, suffix: String)(implicit sqlContext: SQLContext): DataFrame = {
    import sqlContext.implicits._
    groupedData.agg(
      avg($"AverageTemperature").alias(s"${prefix}AverageTemperature${suffix}"),
      max($"AverageTemperature").alias(s"${prefix}MaxTemperature${suffix}"),
      min($"AverageTemperature").alias(s"${prefix}MinTemperature${suffix}")
    )
  }

  def main(args: Array[String]) {
    val conf = new SparkConf()
      .setAppName("TEXHOCEPB Application")

    val sc = new SparkContext(conf)

    sc.setLogLevel("WARN")

    implicit val sqlContext = new SQLContext(sc)

    import sqlContext.implicits._

    val worldDF = loadDataFrame(TEMP_GLOBAL_FILE)
    val countryDF = loadDataFrame(TEMP_BY_COUNTRY_FILE)
    val cityDF = loadDataFrame(TEMP_BY_CITY_FILE)

    val worldAggByYear = aggregateGlobal(worldDF.groupBy($"year"), "World", "PerYear")
    val worldAggByDecade = aggregateGlobal(worldDF.groupBy($"decade"), "World", "PerDecade")
    val worldAggByCentury = aggregateGlobal(worldDF.groupBy($"century"), "World", "PerCentury")

    val countryAggByYear = aggregateLocal(countryDF.groupBy($"year", $"Country"), "Country", "PerYear")
    val countryAggByDecade = aggregateLocal(countryDF.groupBy($"decade", $"Country"), "Country", "PerDecade")
    val countryAggByCentury = aggregateLocal(countryDF.groupBy($"century", $"Country"), "Country", "PerCentury")

    val cityAggByYear = aggregateLocal(cityDF.groupBy($"year", $"Country", $"City"), "City", "PerYear")
    val cityAggByDecade = aggregateLocal(cityDF.groupBy($"decade", $"Country", $"City"), "City", "PerDecade")
    val cityAggByCentury = aggregateLocal(cityDF.groupBy($"century", $"Country", $"City"), "City", "PerCentury")

    val worldJoinedDF = worldAggByYear.alias("w")
      .join(worldAggByDecade, $"w.year".divide(10).cast("integer") === $"decade")
      .join(worldAggByCentury, $"w.year".divide(100).cast("integer") === $"century")

    val countryJoinedDF = countryAggByYear.alias("cy")
      .join(countryAggByDecade.alias("cd"), $"cy.Country" === $"cd.Country" && $"cy.year".divide(10).cast("integer") === $"cd.decade")
      .join(countryAggByCentury.alias("cc"), $"cy.Country" === $"cc.Country" && $"cy.year".divide(100).cast("integer") === $"cc.century")

    val cityJoinedDF = cityAggByYear.alias("ty")
      .join(cityAggByDecade.alias("td"), $"ty.Country" === $"td.Country" && $"ty.City" === $"td.City" && $"ty.year".divide(10).cast("integer") === $"td.decade")
      .join(cityAggByCentury.alias("tc"), $"ty.Country" === $"tc.Country" && $"ty.City" === $"tc.City" && $"ty.year".divide(100).cast("integer") === $"tc.century")

    val resultDF = cityJoinedDF
      .join(countryJoinedDF, $"ty.Country" === $"cy.Country" && $"ty.year" === $"cy.year")
      .join(worldJoinedDF, $"ty.year" === $"w.year")

    val usefulColumns = ("ty.year" :: "ty.Country" :: "ty.City" :: resultDF.columns.toList.filter(c => c.contains("Temperature"))).map(resultDF(_))

    val filteredDF = resultDF.select(usefulColumns: _*)

    filteredDF.show()
    filteredDF.printSchema()

    filteredDF.write.parquet(OUTPUT_PATH)

  }
}