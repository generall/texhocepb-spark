FROM cloudera/quickstart

# copy files from folder to container

# build with:
# docker build -t texhocepb_spark .

# start with:
# docker run --hostname=quickstart.cloudera --privileged=true -t -i --rm texhocepb_spark

RUN mkdir /home/cloudera/data

COPY data/GlobalTemperatures.csv /home/cloudera/data/
COPY data/GlobalLandTemperaturesByCity.csv /home/cloudera/data/
COPY data/GlobalLandTemperaturesByCountry.csv /home/cloudera/data/

COPY target/scala-2.11/technoserv_spark_2.11-1.0.jar /home/cloudera/

COPY launch.sh /usr/bin/

RUN chmod +x /usr/bin/launch.sh

CMD /usr/bin/launch.sh