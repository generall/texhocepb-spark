# TEXHOCEPB test app for interview

Используем файлы:
GlobalTemperatures.csv - среднемесячные температуры в мире
GlobalLandTemperaturesByCountry.csv - среднемесячные температуры по странам
GlobalLandTemperaturesByCity.csv - среднемесячные температуры по городам

Необходимо реализовать на код на spark 1.6 (java 8 или scala), который сформирует датасет (описан ниже) и сохранит его в hdfs в формате parquet.
Предпочтительно использовать dataframe api.

На выходе нужно получить датасет вида:

```
город, страна, год,
средняя температура в городе за год,
минимальная температура в городе за год,
максимальная температура в городе за год,
средняя температура в городе за 10 лет (например, для 1912 года это период 1910-1919 годы),
минимальная температура в городе за 10 лет,
максимальная температура в городе за 10 лет,
средняя температура в городе за век (например, для 1912 года это период 1910-1999 годы),
минимальная температура в городе за век,
максимальная температура в городе за век,
средняя температура в стране за год,
минимальная температура в стране за год,
максимальная температура в стране за год,
средняя температура в стране за 10 лет (например, для 1912 года это период 1910-1919 годы),
минимальная температура в стране за 10 лет,
максимальная температура в стране за 10 лет,
средняя температура в стране за век (например, для 1912 года это период 1910-1999 годы),
минимальная температура в стране за век,
максимальная температура в стране за век,
средняя температура в мире за год,
минимальная температура в мире за год,
максимальная температура в мире за год,
средняя температура в мире за 10 лет (например, для 1912 года это период 1910-1919 годы),
минимальная температура в мире за 10 лет,
максимальная температура в мире за 10 лет,
средняя температура в мире за век (например, для 1912 года это период 1910-1999 годы),
минимальная температура в мире за век,
максимальная температура в мире за век
```


# Getting data

```
mkdir data
cd data
wget https://www.kaggle.com/berkeleyearth/climate-change-earth-surface-temperature-data/downloads/climate-change-earth-surface-temperature-data.zip
unzip climate-change-earth-surface-temperature-data.zip
```

# Building

```
sbt package
```

# Deploying

```
docker build -t texhocepb_spark .
docker run --hostname=quickstart.cloudera --privileged=true -t -i --rm texhocepb_spark
```

# Result schema

```
root
 |-- year: integer (nullable = true)
 |-- Country: string (nullable = true)
 |-- City: string (nullable = true)
 |-- CityAverageTemperaturePerYear: double (nullable = true)
 |-- CityMaxTemperaturePerYear: double (nullable = true)
 |-- CityMinTemperaturePerYear: double (nullable = true)
 |-- CityAverageTemperaturePerDecade: double (nullable = true)
 |-- CityMaxTemperaturePerDecade: double (nullable = true)
 |-- CityMinTemperaturePerDecade: double (nullable = true)
 |-- CityAverageTemperaturePerCentury: double (nullable = true)
 |-- CityMaxTemperaturePerCentury: double (nullable = true)
 |-- CityMinTemperaturePerCentury: double (nullable = true)
 |-- CountryAverageTemperaturePerYear: double (nullable = true)
 |-- CountryMaxTemperaturePerYear: double (nullable = true)
 |-- CountryMinTemperaturePerYear: double (nullable = true)
 |-- CountryAverageTemperaturePerDecade: double (nullable = true)
 |-- CountryMaxTemperaturePerDecade: double (nullable = true)
 |-- CountryMinTemperaturePerDecade: double (nullable = true)
 |-- CountryAverageTemperaturePerCentury: double (nullable = true)
 |-- CountryMaxTemperaturePerCentury: double (nullable = true)
 |-- CountryMinTemperaturePerCentury: double (nullable = true)
 |-- WorldAverageTemperaturePerYear: double (nullable = true)
 |-- WorldMaxTemperaturePerYear: double (nullable = true)
 |-- WorldMinTemperaturePerYear: double (nullable = true)
 |-- WorldAverageTemperaturePerDecade: double (nullable = true)
 |-- WorldMaxTemperaturePerDecade: double (nullable = true)
 |-- WorldMinTemperaturePerDecade: double (nullable = true)
 |-- WorldAverageTemperaturePerCentury: double (nullable = true)
 |-- WorldMaxTemperaturePerCentury: double (nullable = true)
 |-- WorldMinTemperaturePerCentury: double (nullable = true)

```