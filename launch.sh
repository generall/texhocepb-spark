#! /usr/bin/env bash

DAEMONS="\
    mysqld \
    cloudera-quickstart-init"

if [ -e /var/lib/cloudera-quickstart/.kerberos ]; then
    DAEMONS="${DAEMONS} \
        krb5kdc \
        kadmin"
fi

if [ -e /var/lib/cloudera-quickstart/.cloudera-manager ]; then
    DAEMONS="${DAEMONS} \
        cloudera-scm-server-db \
        cloudera-scb-server \
        cloudera-scm-server-db"
else
    DAEMONS="${DAEMONS} \
        zookeeper-server \
        hadoop-hdfs-datanode \
        hadoop-hdfs-journalnode \
        hadoop-hdfs-namenode \
        hadoop-hdfs-secondarynamenode \
        hadoop-httpfs \
        hadoop-yarn-nodemanager \
        hadoop-yarn-resourcemanager \
        spark-history-server"
fi

for daemon in ${DAEMONS}; do
    sudo service ${daemon} start
done

spark-submit  \
     --class SparkAppTechnoserv \
     --deploy-mode client \
     --master yarn \
     --packages com.databricks:spark-csv_2.11:1.4.0 \
     /home/cloudera/technoserv_spark_2.11-1.0.jar

hdfs dfs -ls /tmp/

exec bash