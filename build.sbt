name := "technoserv_spark"

version := "1.0"

scalaVersion := "2.11.7"


libraryDependencies += "org.apache.spark" %% "spark-core" % "1.6.0"
libraryDependencies += "org.apache.spark" % "spark-sql_2.11" % "1.6.0"
libraryDependencies += "com.databricks" % "spark-csv_2.11" % "1.4.0"
//libraryDependencies += "org.apache.hadoop" % "hadoop-client" % "2.7.0"